* Climate Analysis

Analysis of RS data processed in Google Earth Engine (see code in GEE folder) using R.

** RstStats.R
Data is being sliced and analysed to specific areas and using a wider range of visualisation and stats than is convenient in GEE.

** PlotMaps.R
Script to plot the raster over the outline of Uganda for quick visualisation.

